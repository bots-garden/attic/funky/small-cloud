load './vm/config.rb'

BOX_IMAGE = "bento/ubuntu-19.10"

Vagrant.configure("2") do |config|
  config.vm.box = BOX_IMAGE

  ENV['LC_ALL']="en_US.UTF-8"

  config.vm.define "#{CLUSTER_NAME}" do |node| 

    node.vm.synced_folder "./backups", "/home/vagrant/backups", id:"backups"
    node.vm.synced_folder "./cluster", "/home/vagrant/cluster", id:"cluster"
    node.vm.synced_folder "./vm", "/home/vagrant/vm", id:"vm"
    node.vm.synced_folder "./cluster/storage", "/var/lib/rancher/k3s/storage", id:"storage"

    node.vm.hostname = "#{CLUSTER_NAME}"
    #node.vm.network "public_network", ip:"#{CLUSTER_PUB_IP}",  bridge: "en0: Wi-Fi (Wireless)"
    #node.vm.network "public_network", bridge: "en0: Wi-Fi (Wireless)"
    node.vm.network "private_network", ip: "#{CLUSTER_IP}"

    node.vm.provider "virtualbox" do |vb|
      vb.memory = "#{CLUSTER_MEMORY}"
      vb.cpus = "#{CLUSTER_CPUS}"
      vb.name = "#{CLUSTER_NAME}"
    end

    # vagrant provision --provision-with add-tools
    node.vm.provision "add-tools", privileged: true, type: "shell" , inline: <<-SHELL
      # Add tools
      echo "👋 setup"
      apt-get update
      apt-get install unzip -y
      apt-get install zip -y
      apt-get install jq -y
      apt-get install -y curl
    SHELL

    node.vm.provision "k3s-install", privileged: false, type: "shell", inline: <<-SHELL
      curl -sfL https://get.k3s.io | sh -
    SHELL

    node.vm.provision "get-config", privileged: true, type: "shell" , inline: <<-SHELL
      cat /etc/rancher/k3s/k3s.yaml > /home/vagrant/cluster/k3s.yaml
      sed -i "s/127.0.0.1/#{CLUSTER_IP}/" /home/vagrant/cluster/k3s.yaml
      cat /var/lib/rancher/k3s/server/node-token > /home/vagrant/cluster/node-token.txt
      echo 'cluster_ip="#{CLUSTER_IP}"' > /home/vagrant/cluster/cluster.config
    SHELL
    # kubectl config view --flatten

    # vagrant provision --provision-with hosts
    node.vm.provision "hosts", privileged: true, type: "shell" , inline: <<-SHELL
      # Add entries to hosts file:
      echo "" >> /etc/hosts
      echo '#{CLUSTER_IP} #{CLUSTER_DOMAIN}' >> /etc/hosts
      echo "" >> /etc/hosts
    SHELL

  end

end