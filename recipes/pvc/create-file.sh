#!/bin/sh
export KUBECONFIG=../../cluster/k3s.yaml
kubectl exec volume-test -- sh -c "echo local-path-test > /data/test"
kubectl exec volume-test -- sh -c "cat /data/test"
