#!/bin/sh
export KUBECONFIG=../../cluster/k3s.yaml
NAMESPACE="databases"
kubectl port-forward --namespace ${NAMESPACE} svc/mongodb 27017:27017 &