#!/bin/sh
export KUBECONFIG=../../cluster/k3s.yaml


NAMESPACE="databases"
kubectl describe namespace ${NAMESPACE} 
exit_code=$?
if [[ exit_code -eq 1 ]]; then
  echo "🖐️ ${NAMESPACE} does not exist"
  echo "⏳ Creating the namespace..."
  kubectl create namespace ${NAMESPACE}
else 
  echo "👋 ${NAMESPACE} already exists"
fi

kubectl create -f mongodb.yaml -n ${NAMESPACE}
