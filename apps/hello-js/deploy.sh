#!/bin/sh
if [ -z "${KUBECONFIG}" ]
then
  echo "😡 KUBECONFIG is empty"
  exit 1
fi

if [ -z "${SUB_DOMAIN}" ]
then
  echo "😡 SUB_DOMAIN is empty"
  exit 1
fi

if [ -z "${DOCKER_USER}" ]
then
  echo "😡 DOCKER_USER is empty"
  exit 1
fi


COMMIT_MESSAGE=$1
if [ -z "${COMMIT_MESSAGE}" ]
then
  COMMIT_MESSAGE="👋 update and 🚀 deploy"
fi

git add .; git commit -m "${COMMIT_MESSAGE}"; git push

export CONTAINER_PORT=${CONTAINER_PORT:-8080}
export EXPOSED_PORT=${EXPOSED_PORT:-80}

export APPLICATION_NAME=$(basename $(git rev-parse --show-toplevel))
export TAG=$(git rev-parse --short HEAD)
export BRANCH=$(git symbolic-ref --short HEAD)

export IMAGE_NAME="${APPLICATION_NAME}-${BRANCH}-img"
export IMAGE="${DOCKER_USER}/${IMAGE_NAME}:${TAG}"

# === Build ===
docker build -t ${IMAGE_NAME} .
docker tag ${IMAGE_NAME} ${IMAGE}
docker push ${IMAGE}

export HOST="${APPLICATION_NAME}.${BRANCH}.${SUB_DOMAIN}"

if [ -z "${NAMESPACE}" ]
then
  export NAMESPACE="funky-demo-${BRANCH}"
fi

kubectl describe namespace ${NAMESPACE} 
exit_code=$?
if [[ exit_code -eq 1 ]]; then
  echo "🖐️ ${NAMESPACE} does not exist"
  echo "⏳ Creating the namespace..."
  kubectl create namespace ${NAMESPACE}
else 
  echo "👋 ${NAMESPACE} already exists"
fi

# === Deploy ===
rm ./kube/*.yaml

envsubst < ./deploy.template.yaml > ./kube/deploy.${TAG}.yaml

kubectl apply -f ./kube/deploy.${TAG}.yaml -n ${NAMESPACE}

echo "🌍 http://${HOST}"
