# Small Cloud 🌦️

This project is a Vagrant template to create easyly a mono K3S cluster.

> Don't forget: it's for learning and training 😉

## How to use it

- clone me
- go to `/vm/config.rb` and edit it
  - to change the size requirements of the cluster
  - to change the IP of the VM
- run `./start.sh` to create the VM and the cluster
- at the end of the creation, you can find the `k3s.yaml` file in the `/cluster` directory
  - you can use it to populate the `KUBECONFIG` variable (eg: `export KUBECONFIG=./k3s.yaml`)

## How to start and stop the cluster

- start (again): `./start.sh`
- stop: `./stop.sh`
- if you installed the amazing **K9S** application, you can run from the root of the project with `./k9s.sh`

## As I learn, I record everything

So you will find (and stay tuned):

- in `/commands` some commands I use often
- in `/recipes` some script and manifests
  - to deploy some applications (mongodb, redis, ... more to come)
  - to experiment with kube (volume claim and mount, ...)
- in `/apps` you will find some application samples to deploy on your cluster

